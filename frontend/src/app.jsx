import React, { useEffect, useState } from "react";

import { Formik, Form } from "formik";
import * as Yup from "yup";
import { cloneDeep } from "lodash";
import { DateTime } from "luxon";
import classNames from "classnames";

import CustomField from "./CustomField";

const App = () => {

  const [ users, setUsers ] = useState([]);
  const [ results, setResults ] = useState([]);

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required().min(1),
    surname: Yup.string().required().min(1),
  })

  function submit(values, { resetForm }){
    let tempUsers = cloneDeep(users);

    values['when'] = DateTime.now().toFormat('DDDD HH:mm:ss');
    tempUsers.push(values);
    setUsers(tempUsers);

    resetForm();
  }

  function search(values){
    let term = values['searchTerm'].toLowerCase();
    let found = [];

    users.forEach((u, u_id) => {
      if(u['firstName'].toLowerCase().startsWith(term) || u['surname'].toLowerCase().startsWith(term)){
        found.push(u);
      }
    })

    setResults(found);
  }

  useEffect(() => {
    if(users){
      console.log({users});
    }
  }, [ users ])

  return (
    <div>
      <h1>EVPro Full-stack Test --- Jack Newton</h1>

      <div className="half">
        <div className="wrapper">
          <Formik
            initialValues={{
              "firstName":"",
              "surname":"",
            }}
            validationSchema={validationSchema}
            onSubmit={submit}
          >{({values, errors}) => (
            <Form>
              <div className="row form-row">

                <CustomField
                  label="First Name"
                  name="firstName"
                  className={classNames(
                    errors.firstName && "error",
                    "field"
                  )}
                />
              
              </div>
              <div className="row form-row">
                <CustomField
                  label="Surname"
                  name="surname"
                  className={classNames(
                    errors.surname && "error",
                    "field"
                  )}
                />

              </div>

              <div className="row row-end">
                <button className="button button-submit" type="submit">
                  Submit
                </button>
              </div>

            </Form>
          )}
          </Formik>
        </div>

        {!!users && (
          <table className="spaced">

            <thead>
              <tr>
                <th>First Name</th>
                <th>Surname</th>
                <th>Date and Time added</th>
              </tr>
            </thead>

            <tbody>
              
              {users.map((u, u_id) => (
                <tr>
                  <td>{u['firstName']}</td>
                  <td>{u['surname']}</td>
                  <td>{u['when']}</td>
                </tr>
              ))}

            </tbody>

          </table>
        )}
      </div>

      <div className="half">

        <div className="wrapper">
          <Formik
            initialValues={{
              "searchTerm":"",
            }}
            onSubmit={search}
          >{(values, errors) => (
            <Form>
              <div className="row form-row">
                <CustomField
                  label="Search Term"
                  name="searchTerm"
                  className="field"
                />
              </div>

              <div className="row row-end">
                <button className="button button-submit" type="submit">
                  Search
                </button>
              </div>
            </Form>
          )}</Formik>
        </div>

        {!!results && (
          <table className="spaced">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Surname</th>
                <th>Date and Time added</th>
              </tr>
            </thead>

            <tbody>
              {results.map((r, r_idx) => (
                <tr>
                  <td>{r['firstName']}</td>
                  <td>{r['surname']}</td>
                  <td>{r['when']}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}

      </div>

    </div>
  );
};

export default App;
