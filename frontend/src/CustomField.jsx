import React from "react";
import { Field } from "formik";


export const CustomField = ({ label, name, ...props }) => {
    return(
        <React.Fragment>
            <label htmlFor={name} className="field-label">
                {label}
            </label>

            <Field
                name={name}
                {...props}
            />
        </React.Fragment>
    )
}

export default CustomField;